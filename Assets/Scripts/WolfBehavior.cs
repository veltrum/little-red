﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfBehavior : MonoBehaviour {

    public GameObject acorn;            //variable for squirrel's acorn
    public Transform acornSpawn;        //variable for the acorn's spawn point

    [SerializeField] float fireRate;     //rate at which squirrel throws acorns (seconds)
    //Animator anim;                      //for squirrel's animator

    public int BaseAttackDamage;


    // Use this for initialization
    void Start()
    {
        //anim = GetComponent<Animator>();    //animator reference
        StartCoroutine("ThrowAcorns");      //starts the process for firing cannonballs
    }

    IEnumerator ThrowAcorns()
    {
        yield return new WaitForSeconds(fireRate);  //waits to throw an acorn for a set number of seconds
        while (true)
        {
            Instantiate(acorn, acornSpawn.position, acornSpawn.rotation);   //throws one acorn...
            yield return new WaitForSeconds(fireRate);                      //per (firerate) seconds
            //anim.SetTrigger("attack");                                      //tell animator to play attack animation
        }
    }
}
