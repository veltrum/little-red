﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodpeckerBehavior : MonoBehaviour
{
    public float moveSpeed;         //speed of the object's forward movement
    public float frequency;         //speed of sine movement
    public float magnitude;         //size of sine movement
    public float poopRate;          //rate at which the object poops
    public GameObject poo;          //poop object
    public Transform poopSpawn;     //where the bird's poop will come out

    Vector3 axis;             //direction of wave motion
    Vector3 pos;              //will hold the object's transform
    
    // sets the 2 Vector3 objects
    void Start()
    {
        pos = transform.position;
        axis = transform.up;
        StartCoroutine("Poop");
    }

    /// <summary>
    /// Runs every frame. Moves the object forward at a set speed in the sine wave pattern.
    /// </summary>
    void Update()
    {
        pos += transform.right * Time.deltaTime * moveSpeed;
        transform.position = pos + axis * Mathf.Sin(Time.time * frequency) * magnitude;
    }

    /// <summary>
    /// Every "pooprate" seconds, the object will poop
    /// </summary>
    IEnumerator Poop()
    {
        yield return new WaitForSeconds(poopRate);
        while (true)
        {
            Instantiate(poo, poopSpawn.position, poopSpawn.rotation);
            yield return new WaitForSeconds(poopRate);
        } 
    }
}
