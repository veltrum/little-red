﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Timers;

public class PlayerHealth : MonoBehaviour 
{
	public int startingHealth;					//player's health at level start 
	public int currentHealth;                   //player's health at any point in level
	//public Text healthPercent;					//player's health expressed as a percent
	public Slider healthSlider;					// Reference to the UI's health bar. (need UnityEngine.UI)

	bool isDead;								// Whether the player is dead.
	bool damaged;                               // True when the player gets damaged.
                                                //GameController gameController;

    private Timer playerInvulnerabilityTimer;

	// Use this for initialization
	void Start () 
	{
		currentHealth = startingHealth;         //Start the player off with 100% health
                                                //GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
                                                //gameController = gameControllerObject.GetComponent<GameController> ();

        // Create the timer for which the player is invulnerable after taking damage
        playerInvulnerabilityTimer = new Timer(3000);
        playerInvulnerabilityTimer.Elapsed += Time_Elapsed;
    }

    private void Time_Elapsed(object sender, ElapsedEventArgs e)
    {
        damaged = false;
        playerInvulnerabilityTimer.Stop();
        
        // Player stop blinking here
    }

    // Update is called once per frame
    void Update () 
	{
		//Show the player's health with the bar and percent
		healthSlider.value = currentHealth;
		//healthPercent.text = currentHealth + "%";
	}

    public void TakeDamage(int amount)
    {
        if (!damaged)
        { 
            //sets damage flag to true
            damaged = true;

            //decrements health by the amount of damage the player just took
            currentHealth -= amount;

            //shows the remaining health
            healthSlider.value = currentHealth;
            //healthPercent.text = currentHealth + "%";

            //Once the player's health reaches zero
            if (currentHealth <= 0 && !isDead)
            {
                PlayerDeath();      //kill the player
            }

            playerInvulnerabilityTimer.Start();

            // Player start blinking here
        }
    }

	public void PlayerDeath()
	{
		isDead = true;  					    //player is dead 
		//Destroy(gameObject);						
        SceneManager.LoadScene("Level");		//reload the level
	}

    /// <summary>
    /// This is called when the player comes incontact with an enemy
    /// </summary>
    /// <param name="other">Other colider2D</param>
    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            if (null != other.gameObject.GetComponent<WolfBehavior>())
            {
                TakeDamage((other.gameObject.GetComponent<WolfBehavior>().BaseAttackDamage));
            }
            else
            {
                TakeDamage(5);
            }
        }
    }
}
