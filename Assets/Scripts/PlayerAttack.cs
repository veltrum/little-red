﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour 
{
	public GameObject apple;          //reference to apple
	public float fireRate;            //rate of fire for shots
    public GameObject appleSpawn;     //where the apples are shot from

    float nextFire;					  //when player can throw another apple
	GameObject player;				  //reference to player gameObject
	
	Vector3 offset;					  //follows player
	//Animator anim;					  //variable for player's animator
	PlayerMovement playerMovement;	  //reference to player's movement script

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		//anim = GetComponent<Animator> ();
		playerMovement = player.GetComponent<PlayerMovement> ();
		offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButton ("Fire1") && Time.time > nextFire) 	//Player left clicks...
		{
			nextFire = Time.time + fireRate;	 //controls the fireRate using game time
			//anim.SetTrigger("attack");			 //tells animator to play the attack animation

			if (playerMovement.isRight)	//if player is facing right
			{
				Instantiate (apple, appleSpawn.transform.position, Quaternion.Euler (new Vector3 (0, 0, 0)));  //Apple is thrown right
			}
			else 
			{
				Instantiate (apple, appleSpawn.transform.position, Quaternion.Euler (new Vector3 (0, 180, 0)));  //Apple is thrown left
			}
		}

		//follow the player
		transform.position = player.transform.position + offset;
	}
}
