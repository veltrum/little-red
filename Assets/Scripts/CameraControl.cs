﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    ///Although the original camera for Red Riding Hood: Vengeance of the Wolf 
    ///was acceptable, the redux should correct the issues it had. To fix the issues
    ///and have a camera worthy of a redux, this script should enable the camera
    ///to do 4 things:
    ///1) follow the player
    ///2) allow zone for player to move without following
    ///3) stay on player as he moves down
    ///4) linearly snap up when player jumps on high platform
    ///
    ///ALL of the 4 things need to be done
    ///Inspiration is derived from Super Mario World

    Transform target;
    [SerializeField] float minX;
    [SerializeField] float minY;
    [SerializeField] float maxX;
    [SerializeField] float maxY;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void LateUpdate()
    {
        transform.position = new Vector3(Mathf.Clamp(target.position.x, minX, maxX), Mathf.Clamp(target.position.y + 3, minY, maxY), transform.position.z);
    }
}
