﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PorcupineBehavior : MonoBehaviour
{
    public float speed;     //speed at which porcupine moves
    public float turnTime;  //time at which porcupine turns around

    float travelTime;       //increments to turnTime
    Rigidbody2D rb;         //porcupine's rigidbody
    
    /// <summary>
    /// Used for initialization.
    /// </summary>
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        travelTime = 0f;
    }

    /// <summary>
    /// Happens once per frame. Moves the porcupine forward, then backward 
    /// after some time by negating the speed variable
    /// </summary>
    void Update()
    {
        rb.velocity = new Vector2(transform.position.x * speed, rb.velocity.y);
        travelTime += Time.deltaTime;

        if (travelTime >= turnTime)
        {
            speed *= -1f;
            travelTime = 0f;
        }
    }
}
