﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour 
{
	public int startingHealth;					//variable for enemy's starting health
	public int currentHealth;					//variable for enemy's current health

	bool damaged;								//variable to tell the game if the enemy has been damaged
	bool isDead;								//variable to tell the game if the enemy is dead
	//Animator anim;								//reference to enemy's animator

	void Start () 
	{
		//anim = GetComponent<Animator> ();	//squirrel's animator reference
		currentHealth = startingHealth;		//start squirrel at full health
	}
	
	// Update is called once per frame
	void Update () 
	{
		damaged = false;						//by default, the squirrel is not taking damage
	}

	public void TakeDamage (int amount)
	{
		damaged = true;						//squirrel takes damage
		//anim.SetTrigger ("damaged");		//animator plays hurt animation (named squirrel_death)

		if(isDead)
		{
			return;  //the function should exit here if the enemy is dead
		}

		currentHealth -= amount;  //decrement the enemy's health by the damage inflicted by the player

		//if the enemy's health is zero
		if(currentHealth <= 0)
		{
			isDead = true;  		//the enemy dies
			Destroy(gameObject);	//remove the enemy
		}
	}
}
