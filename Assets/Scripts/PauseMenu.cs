﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] Canvas pauseMenu;
    [SerializeField] Button resume;
    [SerializeField] Button returnToMenu;

    bool isPaused;                    //whether or not the game is false
    PlayerMovement playerMovement;	  //reference to player's movement script
    PlayerAttack playerAttack;	      //reference to player's attack script

    // Use this for initialization
    void Start()
    {
        isPaused = false;           //At game's start, game is not paused
        pauseMenu.enabled = false;

        //set player script references
        playerMovement = gameObject.GetComponent<PlayerMovement>();
        playerAttack = gameObject.GetComponent<PlayerAttack>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))       //if the ESC key is pressed
        {
            isPaused = true;     //toggle the isPaused's value
        }

        if (isPaused == true)        //if the game is paused
        {
            Time.timeScale = 0;         //stop the game's time

            //disable the player's scripts
            playerMovement.enabled = false;
            playerAttack.enabled = false;

            pauseMenu.enabled = true;
        }
        else    //otherwise
        {
            Time.timeScale = 1;             //restart the game's time

            //enable the player's scripts
            playerMovement.enabled = true;
            playerAttack.enabled = true;
        }
    }

    public void NoPress()
    {
        pauseMenu.enabled = false;
        isPaused = false;
    }

    public void ReturnToMenuPress()
    {
        //Starts up the Red Level
        SceneManager.LoadScene("MainMenu");
    }
}
