﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    public Canvas MainCanvas;       //for the main menu
    public Canvas ExitCanvas;       //for the submenu that allows for quitting the game
    public Canvas HowtoCanvas;      //for the How to Play page
    public Button PlayGame;         //Play Game button
    public Button HowToPlay;        //How to Play button
    public Button ExitGame;         //Exit Game button

    // Use this for initialization
    void Start()
    {
        //reference setup
        ExitCanvas = ExitCanvas.GetComponent<Canvas>();
        HowtoCanvas = HowtoCanvas.GetComponent<Canvas>();
        ExitGame = ExitGame.GetComponent<Button>();
        HowToPlay = HowToPlay.GetComponent<Button>();
        PlayGame = PlayGame.GetComponent<Button>();

        //disables submenu that allows for exit
        ExitCanvas.enabled = false;
        HowtoCanvas.enabled = false;
    }

    //If player presses the "Play Game" button
    public void PlayGamePress()
    {
        //Starts up the Red Level
        SceneManager.LoadScene("Level");
    }

    public void HowToPlayPress()
    {
        HowtoCanvas.enabled = true;     //Displays the How to Play page
        ExitCanvas.enabled = false;     //Disables the exit game submenu

        //Disables the buttons on the main menu
        ExitGame.enabled = false;
        HowToPlay.enabled = false;
        PlayGame.enabled = false;
    }

    //If player presses the "Exit Game" button
    public void ExitGamePress()
    {
        HowtoCanvas.enabled = false;        //Disables the How to Play page
        ExitCanvas.enabled = true;          //Displays the exit game submenu

        //Disables the buttons on the main menu
        ExitGame.enabled = false;
        HowToPlay.enabled = false;
        PlayGame.enabled = false;
    }

    //If the player stays in the game
    public void NoPress()
    {
        ExitCanvas.enabled = false;     //Hides the exit game submenu
        HowtoCanvas.enabled = false;    //Hides the How to Play page

        //Enables the buttons on the main menu
        ExitGame.enabled = true;
        HowToPlay.enabled = true;
        PlayGame.enabled = true;
    }

    //When the player wants to leave
    public void QuitGame()
    {
        Application.Quit();         //Quits the game
    }
}
