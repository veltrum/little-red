﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
	public float moveSpeed;							//controls Player's movement speed
	//[HideInInspector] public float currentSpeed;	//variable for the Player's current speed
	[HideInInspector] public bool isGrounded;		//variable for telling whether or not the player is on the ground
	[HideInInspector] public bool isRight;			//variable for telling the game whether the player is facing right or not
	public float jumpHeight;						//controls how high the player can jump
	public GameObject appleSpawn;					//spawns apples at player's command

	Transform groundCheck;		//variable for the transform below the player's feet
	Rigidbody2D rb;				//variable will look for component within GameObject
	Animator anim;				//variable for player's animator
    PlayerHealth health;

	// Use this for initialization
	void Awake () 
	{
		//reference setup
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		groundCheck = transform.Find("GroundCheck");
		isRight = true;
		isGrounded = false;

        health = GetComponent<PlayerHealth>();
	}

	// Runs once per frame
	void Update()
	{
		// The player is grounded if a linecast to the groundcheck object's position hits anything on the ground layer.
		isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground")); 

		Jump();					//function for allowing the player to jump
	}

	// Update function for physics
	void FixedUpdate () 
	{
		float horizontal = Input.GetAxis ("Horizontal"); 						//left and right arrow keys, A, and D control movement along this axis
		rb.velocity = new Vector2 (horizontal * moveSpeed, rb.velocity.y); 		//moves Player left or right
		anim.SetFloat("speed", Mathf.Abs(horizontal));							//sends float value to animation controller

		if (rb.velocity.y > 1 && !isGrounded) 		//if player is going up and not on the ground
		{
			anim.SetBool ("jump", true);	//activate player jump animation
		} 
		else if (rb.velocity.y < 1 && !isGrounded)	//if player is going down and not on the ground
		{
			anim.SetBool ("fall", true);	//activate player falling animation
			anim.SetBool ("jump", false);	//deactivate player jumping animation
		} 
		else 		//if player is not going up nor down
		{
			//deactivate both of these animations
			anim.SetBool ("fall", false);
			anim.SetBool ("jump", false);
		}

		Turn(horizontal);						//function for turning the player
		//currentSpeed = rb.velocity.magnitude;	//gets player's current speed
	}

	//function for turning the player
	void Turn (float horizontal)
	{
		//flips player left or right depending on the value of the x scale value
		if (horizontal > 0 && !isRight || horizontal < 0 && isRight) 
		{ 
			isRight = !isRight;								//appropriately change the bool value
			Vector3 playerScale = transform.localScale;		//make a vector3 to hold the player's current scale value
			playerScale.x *= -1f;							//flip that vector3 along the x-axis
			transform.localScale = playerScale;				//apply the vector3 to the player's scale

			//Do a similar method with the appleSpawn object
			Quaternion appleOffset = appleSpawn.transform.localRotation;
			appleOffset.y *= -1f;
			appleSpawn.transform.localRotation = appleOffset;
		} 
	}

	//function for allowing the player to jump
	void Jump()
	{
		//if the player presses the spacebar an the GameObject is on the ground
		if (Input.GetButtonDown ("Jump") && isGrounded) 
		{						
			rb.AddForce(new Vector2(0f, jumpHeight));		//propels player upward at the force of the jumpHeight variable
		}
	}
}
