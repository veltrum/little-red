﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	public float speed;					//speed the projectile moves
	public int damage;					//damage the projectile inflicts

	Rigidbody2D rb;						//projectile's rigidbody component
	[SerializeField] float lifeTime;	//amount of time projectile is in the scene before it's destroyed

	// Use this for initialization
    /// <summary>
    /// Destroys the projectile after a certain time,
    /// references the rigidbody component, and will 
    /// give the projectile forward speed if it's an apple or an acorn
    /// </summary>
	void Start () 
	{
		Destroy (gameObject, lifeTime);
        rb = GetComponent<Rigidbody2D>();
        if (gameObject.tag == "Apple" || gameObject.tag == "Acorn")
        {
            rb.velocity = transform.right * speed; 
        }
    }

	void OnTriggerEnter2D(Collider2D other)
	{
		if (gameObject.tag == "Apple") 	//if the object this script is attached to is an apple
		{
			if(other.gameObject.tag == "Enemy")		//if the apple hits an enemy
			{
				//Check if it has the EnemyHealth script
				EnemyHealth enemyHealth = other.gameObject.GetComponent<EnemyHealth> ();

				//if the EnemyHealth script exists
				if (enemyHealth != null) 
				{
					enemyHealth.TakeDamage (damage);		// damage the enemy.
				}
                  
				Destroy (gameObject);	//destroy apple
			}
		}

		if (gameObject.tag == "Acorn" || gameObject.tag == "Poop") 	//if this object is an acorn
		{
            if (other.gameObject.tag == "Player") //if the acorn hits the player
			{	
				//Check if it has the EnemyHealth script
				PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth> ();

				//if the EnemyHealth script exists
				if (playerHealth != null) 
				{
					playerHealth.TakeDamage (damage);		// damage the enemy.
				}

				Destroy (gameObject);	//destroy acorn
			} 
			else if (other.gameObject.tag == "Enemy") //if the acorn hits an enemy
			{
			}
		}
	}
}